import java.util.ArrayList;
import java.util.List;

public class TreeNumbersComposite extends TreeNumbers{
    private String nombre;
    private java.util.List<TreeNumbers> List;

    public TreeNumbersComposite(String nombre){
        this.nombre=nombre;
        this.List=new ArrayList<TreeNumbers>();
    }

    @Override
    public void add(TreeNumbers treeNumbers) {
        assert treeNumbers != null;
        this.List.add(treeNumbers);
    }

    @Override
    public void remove(TreeNumbers treeNumbers) {
        assert treeNumbers != null;
        this.List.remove(treeNumbers);
    }

    @Override
    public int sum() {
        int result= 0;
        for(TreeNumbers treeNumbers : this.List){
            result += treeNumbers.sum();
        }
        return result;
    }

    @Override
    public int higher() {
        int result=Integer.MIN_VALUE;
        for (TreeNumbers treeNumbers : this.List){
            int higher= treeNumbers.higher();
            if(higher>result){
                result=higher;
            }

        }
        return result;
    }

    @Override
    public int numberOfTreeNumbers() {
        return this.List.size();
    }

    @Override
    public String toString() {
        return "["+this.nombre+"]";
    }
}
